+++
title = "Jobcenter"
description = ""
weight = 20
+++

Jobs des Jobcenters
======
Alle Jobs des Jobcenters funktionieren auf dem selben Prinzip. Zunächst müsst ihr euren gewünschten Job annehmen.

Auf der Karte ist nun ein Symbol für die Umkleidekabine und gegebenenfalls die Garage des jeweiligen Jobs markiert (SYMBOLE HIER EINFÜGEN). Erster Schritt wäre dort hinzufahren. Bei vielen Jobs wird die Arbeitsuniform verlangt, ohne diese könnt Ihr den Job leider nicht ausführen.

Falls Ihr noch kein eigenes Fahrzeug besitzt oder euer Kofferraum zu klein ist, könnt ich euch für etwas Kleingeld, einen Firmenwagen ausparken. Sobald Ihr die oben aufgeführten Anforderungen erfüllt, kann es auch schon losgehen. Anbei findet ihr noch einmal die Abfolge der einzelnen Berufe.
**INFO:** *Aktuell kann der Kofferraum von Firmenfahrzeugen nicht genutzt werden. Wir arbeiten bereits an einer Problemlösung.*

Abfolge der einzelnen Berufe
------
#### Fischer
Zunächst begebt ihr euch zur Fischereiumkleide und legt eure Arbeitskleidung an. Nun fahrt Ihr zum Fischereihafen und leiht euch ein Boot aus. Fahrt das Boot nun zum Fischereigebiet und fangt ein paar Fische. Diese könnt Ihr im Anschluss beim Lieferort verkaufen. Pro Fisch erhältst du 11 $, du kannst maximal 100 Fische bei dir tragen.

#### Tankstellenlieferant
Für diesen Job musst du dich nicht zwingend umziehen. Fahre also nach Bedarf zur Umkleide, danach weiter zum Ölbohrer. Dort sammelst du zunächst Öl, welches du im nächsten Schritt beim Ölmischer verarbeiten musst. Hierbei erhältst du jeweils _ein_ bearbeitetes Öl für _zwei_ Öl. Fahre nun weiter zum Ölveredler, dort veredelst du das bearbeitete Öl zu Benzin. Pro _bearbeitetes Öl_ erhältst du _zwei_ Benzin. Im Anschluss kannst du das erhaltene Benzin abliefern. Pro Benzin erhältst du 62 $, du kannst maximal 24 Benzin bei dir tragen.

#### Schneider
Als Schneider begibst du dich zunächst zur Umkleide, und legst deine Arbeitskleidung an. Nun geht es weiter zum Wollefeld um reine Wolle zu sammeln. Diese kannst du später im Hauptraum der Umkleide weiter zu Stoff verarbeiten. Den erhaltenen Stoff kannst du an der Nähstation nun zu Kleidung verarbeiten und im Anschluss verkaufen. Pro Kleidung erhältst du 25 $, du kannst maximal 100 Kleidungsstücke bei dir tragen.

#### Schlachter
Als Schlachter musst du zunächst in der Nähe vom Airport deine Arbeitskleidung anziehen. Begebe dich nun zur Hühnerfarm, und fange ein paar _lebendige Hühner_ ein. Diese kannst du in der gleichen Fabrik schlachten und anschließend verpacken. Folge hierzu einfach den Gängen weiter nach hinten. Die _verpackten Hühner_ kannst du nun beim Cluckin' Bell in der Innenstadt abliefern. Pro verpacktem Hühnchen erhältst du 20 $, du kannst maximal 100 verpackte Hühnchen bei dir tragen.

#### Minenarbeiter
Begebe dich zunächst zur Bergarbeiterumkleide und lege deine Arbeitskleidung an. Begebe dich nun zum Steinbruch und sammle ein paar Steine. Diese fährst du im Anschluss zur Steinwäsche und reinigst diese. Weiter gehts zur Steinschmelze, die dir aus den einzelnen Steinen verschiedene Erze gibt. Diese Erze kannst du im Anschluss bei den jewiligen Verkäufern gegen gewisse Summen eintauschen. Pro Kupfer erhältst du 5 $, hiervon kannst du 56 maximal bei dir tragen. Für jedes Eisen erhältst du 10 $, hier kannst du maximal 42 Stück tragen. Gold verkauft sich für 25 $, wovon du maximal 25 Stück tragen kannst. Manchmal erhältst du sogar Diamanten, welche du für 150 $ verkaufen kannst. Maximal 50 Diamanten passen in deine Tasche.