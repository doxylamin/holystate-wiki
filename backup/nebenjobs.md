+++
title = "Nebenjobs"
description = ""
weight = 20
+++

Nebenjobs
======
Die verfügbaren Nebenjobs funktionieren ähnlich, wie die des Jobcenters. Der einzige Unterschied ist, dass du keinen Arbeitsvertrag eingehst und es dir demnach freisteht, einer weiteren Organisation anzugehören. Für diese Jobs kannst du dir selbst aussuchen, worauf du gerade Lust hast.

Mit folgenden Gegenständen kannst du innerhab der Nebenjobs Geld verdienen:
* Holz (zu verpackten Brettern, pro Einheit zwischen 32 und 48 $)
* Weizen (zu Mehl, pro Einheit zwischen 22 und 38 $)
* Trauben (zu Wein, pro Einheit zwischen 45 und 64 $)

Liefere die verarbeiteten Einheiten nun zum Costco Großmarkt <img src="/assets/blips/radar_chinese_strand.png" style="display:inline; height: auto; margin: auto; -webkit-filter: invert(40%) grayscale(100%) brightness(80%) sepia(100%) hue-rotate(5deg) saturate(500%) contrast(2);">, dieser kauft die jeweils zu den oben genannten Preisen an.

Pfandflaschen
======
Ebenfalls kannst du jederzeit auf die Suche nach Pfandflaschen gehen. Diese kannst du in sämtlichen Mülleimern im Staate finden. Dein Glück entscheidet, wie viele du in einem Mülleimer findest. An einigen Läden gibt es die Möglichkeit, diese abzugeben. Die Abgabestellen findest du auf der Karte mit einem pinken <img src="/assets/blips/radar_last_team_standing.png" style="display:inline; height: auto; margin: auto; -webkit-filter: invert(40%) grayscale(100%) brightness(40%) sepia(100%) hue-rotate(-85deg) saturate(500%) contrast(2);"> markiert. Pro Pfandflasche erhältst du zwischen 10 $ und 25 $.