---
title: Home
---

HolyState Wiki
===================

Herzlich Willkommen im Wiki von HolyState - auf den folgenden Seiten findest du unser Regelwerk, einige Tipps und Tricks sowie einen Leitfaden für unser Whitelisting.

Bei Fragen oder Problemen kannst du dich gerne im [Discord](https://discord.gg/holystate) bei uns melden.

<span style="color: red;">「Der Administration & Projektleitung ist es vorbehalten, einzelne Punkte oder gesamte Abschnitte nach interner Absprache zu erweitern, verändern und zu entfernen.」</span>


<div style="font-size: 0;">
  <div style="width: 50%; vertical-align: top; display: inline-block; font-size: 1.05rem; padding-right: 5px;">
    <div style="border: 1px solid lightgray; padding: 15px; margin-bottom: 5px;">
      <h4>Regelwerk</h4>
      <ul>
        <li><a href="/regelwerk/allgemein">Allgemeines</a></li>
        <li><a href="/regelwerk/charakter">Charakterrichtlinien</a></li>
        <li><a href="/regelwerk/fraktionen">Fraktionen und Gruppierungen</a></li>
        <li><a href="/regelwerk/kommunikation">Kommunikation</a></li>
        <li><a href="/regelwerk/raeube">Geiselnahmen, Bank- und Ladenräube, Überfälle</a></li>
        <li><a href="/regelwerk/sonderregelungen">Sonderregelungen</a></li>
        <li><a href="/regelwerk/streaming">Richtlinien für Streaming und Aufnahmen</a></li>
        <li><a href="/regelwerk/waffen">Umgang mit Waffen</a></li>
        <li><a href="/regelwerk/zonen">Sichere Zonen, Sperrzonen, etc.</a></li>
      </ul>
    </div>
    <!--<div style="border: 1px solid lightgray; padding: 15px; margin-bottom: 5px;">
      <h4>Gesetzbuch</h4>
      <ul>
        <li><a href="#">Grundgesetz</a></li>
        <li><a href="#">Strafgesetzbuch (StGB)</a></li>
        <li><a href="#">Straßenverkehrsordnung (StVO)</a></li>
      </ul>
    </div>-->
  </div>
  <div style="width: 50%; vertical-align: top; display: inline-block; font-size: 1.05rem;">
  <div style="border: 1px solid lightgray; padding: 15px; margin-bottom: 5px;">
      <h4>Tipps und Tricks</h4>
      <ul>
        <li><a href="/tipps_und_tricks/fortbewegungsmittel/">Fortbewegungsmittel</a></li>
        <li><a href="/tipps_und_tricks/ganganmeldungen/">Fraktionsbewerbungen</a></li>
        <li><a href="/tipps_und_tricks/grundstuecke/">Häuser & Grundstücke</a></li>
        <li><a href="/tipps_und_tricks/illegales/">Illegale Aktivitäten</a></li>
        <li><a href="/tipps_und_tricks/jobcenter/">Jobcenter</a></li>
        <li><a href="/tipps_und_tricks/tastenbelegung/">Tastenbelegungen</a></li>
      </ul>
    </div>
    <div style="border: 1px solid lightgray; padding: 15px; margin-bottom: 5px;">
      <h4>Links</h4>
      <ul>
        <li><a href="https://holystate.de">Webseite</a></li>
        <li><a href="https://discord.gg/holystate">Discord</a></li>
        <li><a href="fivem://connect/45.11.16.174">FiveM</a></li>
      </ul>
    </div>
  </div>
</div>