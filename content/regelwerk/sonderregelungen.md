+++
title = "Sonderregelungen"
description = ""
+++

- Trifft der Fall ein, dass die Mediziner einen Spieler vor Ort durch einen Bug, nicht sehen können und somit keine Behandlung stattfinden kann und der Mediziner im Support bestätigt
dass sich dort ein Dispatch ohne Patient befindet, so ist der Mediziner **verpflichtet** bis zur administrativen Wiederbelebung vor Ort zu bleiben und **entsprechende Behandlungsmaßnahmen** durchzuführen.

- Für Mitglieder des Server Teams gilt, dass diese keine Support-Fälle bearbeiten **dürfen** , in denen sie selbst, ihre Fraktion / Gruppierung oder Unternehmen befangen sind. Bei derartigen Fällen, **werden** diese an eine dritte Partei weitergeleitet und dementsprechend auch, **ausschließlich** von dieser weiterbearbeitet.

- Der Administration ist es vorbehalten, einzelne Punkte oder gesamte Abschnitte nach interner Absprache zu erweitern, verändern, zu entfernen oder dessen Interpretation auf die Situation anzupassen.

- Das Ansprechen der Teammitglieder im Roleplay, um auf Missstände aufmerksam zu machen, ist strengstens verboten. Hierfür gibt es einen TeamSpeak Support. Hierunter fällt auch **"Hör auf, ich sag das gleich LeuchTii!!11".**

{{% alert theme="danger" %}}<center><b>Unwissenheit schützt nicht vor Strafe</b></center>{{% /alert %}}