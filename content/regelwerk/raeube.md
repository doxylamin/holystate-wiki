+++
title = "Überfälle, Geiselnahmen, Räube"
description = ""
+++

- Ein Bankraub gilt als Großaktion und benötigen **mindestens 6 aktive Polizisten** im Dienst.

- Geiselnahmen sind erst ab **ab 6 aktiven Polizisten** möglich.

- Ein Ladenraub ist **ab 3 aktiven Polizisten** im Dienst verfügbar.

- Im Allgemeinen gilt die Regel **Polizisten im Dienst +2** für die Anzahl der agierenden Gruppierungsmitgliedern.

- Eine Sonderregelung der Geiselnahme ist, dass **maximal $ 30.000** pro Geisel verlangt werden dürfen. Das Maximum welches durch eine Geiselnahme erwirtschaftet werden darf, beträgt
 **höchstens $100k** , welches überraschenderweise einem **Maximum** von **5 Geiseln** entspricht.

- Die Staatsbank kann mit Update 1.8.0 nur ein mal pro Sonnenwende ausgeraubt werden.

- Die Person, die den Raub durchführt, darf sich während dieser läuft nicht vom Ort des Geschehens entfernen. _Du kannst also nicht auf das Dach des Gebäudes klettern und weiter rauben_.

- Das Zwingen von Spielern ihr Konto zu plündern oder Zwangsüberweisungen zu tätigen ist **absolut** untersagt und gilt als immenses Fehlverhalten. Zudem brecht ihr einer der Grundregeln des Roleplays. Solche Aktionen, sowie jemanden die Entscheidungsfreiheit zu nehmen und sein Leben und Vermögen anderweitig nicht schützen zu können fällt unter die Kategorie Power-RP und wird mit bis zu **einem permanenten** Bann bestraft.

- Das Stehlen von Fahrzeugen ist, sofern es die RP-Situation gestattet und ein RP Hintergrund besteht, erlaubt. **Das Überschreiben von Fahrzeugen über das UCP darf nicht erzwungen werden und fällt unter Power-RP.**

# Anzeige der aktiven Polizisten im Spiel
- 0 Beamte: ⚫
- 1 bis 3 Beamte: 🔴
- 4 bis 6 Beamte: 🔵️
- über 7 Beamte: ⚪