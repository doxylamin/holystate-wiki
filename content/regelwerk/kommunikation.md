+++
title = "Kommunikation"
description = ""
+++

- Achtet darauf, dass **Salty Chat** aktiviert ist und eure Geräte richtig erkannt wurden, damit ihr vorhersehbare Probleme eindämmen könnt.

- Ordentliche Qualität eures Mikrofons ist das A und O um ein angenehmes Klima im Roleplay zu schaffen.

- Stellt sicher, dass Störgeräusche (z. B. Rauschen, statisches Knistern, Rückkopplungen, nervige Geschwister oder anstrengende Eltern, etc.) unterbunden werden und zu unterlassen sind.

- Gefesselt gibt es **keine** Möglichkeiten auf Gerätschaften der Kommunikation (z. B. Handy, Funkgeräte, In-Game-Chats) zurückzugreifen.

- Aussagen oder Andeutungen, die supportbetreffend ausgesprochen werden, wie beispielsweise &quot; Wir sehen uns im Support / der Suppenküche&quot; sind **nicht** gestattet.
Diese sind nur unnötig aufbrausend und in einer RP-Situation Fehl am Platz. (siehe Allgemeines)

- Das übermäßige Abspielen von Musik, sowie die übermäßige Nutzung von Soundboards ist im Roleplay nicht gestattet. Ausnahme hiervon ist die Nutzung von Soundboards um Aktionen im Roleplay zu untermalen. Zum Beispiel als Mechaniker beim Reifenwechsel einen Akkuschrauber-Sound abzuspielen geht vollkommen in Ordnung.

- Alle Chatfunktionen (**auch Voice Chat**) im Spiel sind In-Character zu betrachten. Solltet Ihr die Chatfunktion für unnötige Beleidigungen, Out-Of-Character oder gar als Troll-Plattform nutzen, kann dies mit einem Warn bis hin zum **24 Stunden Bann** führen.

- HolyState ist ein **deutscher** Roleplay-Server! Es sollte sich herauskristallisieren, dass
somit in allen Chats die deutsche Sprache anzuwenden ist. Kleinere Abschweifungen in anderen Sprachen sind gestattet, sofern es ins Roleplay und zu eurem Charakter passt.

**Schussankündigungen müssen auf Deutsch erfolgen!**