+++
title = "Grüne und Rote Zonen"
description = ""
+++

- Auf unseren Server gibt es exakt drei grüne Zonen, diese befinden sich am Spawn, am Krankenhaus, sowie im Innenraum des Gefängnisses. Innerhalb dieser  **dürfen** keine Waffen gezogen werden. Diese Regel entfällt sofern eine Partei in eine grüne Zone flüchtet. Eine Sonderregelung gilt für Bankautomaten / ATMs, diese gelten als passive Safezones, in denen zwar **keine** Situation gestartet werden **darf**, jedoch die Nutzung von Waffen gewissermaßen erlaubt ist. Der Zonenbereich liegt circa **5 Meter** um den Bankautomaten. Sofern jemand in eine grüne Zone flüchtet, ist diese für die laufende RP Situation aufgehoben.

- Rote Zonen stellen sogenannte Ganggebiete dar, in welchen u.a. keine verbale Schussankündigung gemacht werden muss. Innerhalb dieser Zonen reicht das dreimalige Schießen in die Luft, um als Schussankündigung wahrgenommen zu werden. Mediziner sind in dieser Zone unantastbar und dürfen nicht verletzt werden. Weiterhin können keine permanenten Tode verübt werden.