+++
title = "Umgang mit Waffen"
description = "Nutzung und Umgang mit Waffen, Schussankündigungen, etc."
+++

## Nutzung von Waffen

- Es gilt die Regel, dass Spielern, sollten diese Langwaffen bei sich tragen, nur dessen Waffen entzogen werden dürfen, wenn diese eine Tragetasche tragen bzw. die RP-Situation dies erlauben (z. B. hat diese in den Kofferraum gepackt oder vorher gezeigt und in einen Müllcontainer oder Gebüsch geschmissen / versteckt). Zusätzlich dürfen bei Kontrollen oder Raubzügen / Überfällen den beraubten Spielern **maximal** **eine** Langwaffe entnommen werden. Auch wenn diese mehrere bei sich tragen sollten. Die verbleibenen Waffen dürfen dennoch nicht in der selben Situation genutzt werden.

- Die Nutzung eines Tazers gilt nicht als Schussankündigung. Weiterhin darf dieser nicht genutzt werden, sofern man mit einer überlegeneren Waffe bedroht wird.

- Waffen (z. B. Langwaffen, Taser, etc. der Polizei), Gegenstände (Bandagen, Medikits) und Fahrzeuge von Staatsfraktionen dürfen **nicht für eigene Zwecke** verkauft oder verschenkt werden. Innerhalb einer RP Situation (u.a. Geiselnahme) dürfen diese jedoch als Forderung geltend gemacht werden. Fahrzeuge dürfen nicht dauerhaft überschrieben / verkauft werden.

- Der Polizei und anderen <a nohref title="Behörden und Organisationen mit Sicherheitsaufgaben">BOS-Fraktionen</a> dürfen nicht um Ihre Dienstwaffen beraubt werden. Sofern ein Beamter jedoch eine Waffe beschlagnahmt und bei sich trägt, kann diese wieder entwendet werden.

## Schussankündigungen

- Der Start bzw. die Durchführung eines Raubüberfalls gilt nicht als Schussankündigung.

- Schussankündigungen müssen zwingend auf **Deutsch** erfolgen.

- Der Beschuss von Reifen ist nur dann zulässig, sofern zuvor die verbale Kontaktaufnahme gescheitert ist. Die Partei im beschossenen Fahrzeug _darf jedoch das Feuer erwiedern_.

- Eine Ankündigung ist nicht notwendig, sofern zuvor von der Gegenpartei bereits angekündigt wurde. Hier gilt immer _Aktion gleich Reaktion_, auf eingehende Schläge sollte _nicht sofort_ mit Beschuss reagiert werden.