+++
title = "Fraktionen / Gruppierungen"
description = ""
+++

- Probleme die durch Fraktionen oder Gruppierungen euren oder den allgemeinen Spiel-, RP-Fluss beeinträchtigen sind zunächst mit dessen Oberhäupten zu klären. Sollte dies nicht möglich sein, kann das Serverteam hinzugezogen werden.

- Fraktionen und Gruppierungen dürfen **ab** 5 Spielern und einem gut ausgedachten Konzept dem Serverteam vorgestellt werden. Diese dürfen **maximal 10 Mitglieder** umfassen.
Staatsfraktionen (LSMC, LSPD, LSAC etc.) verfügen über eine Kapazität von **25 Mitgliedern**. Jegliche Art Anwärter ( Arbeiter, Runner, Untergeordnete, Butler) zählen als **vollständiges Mitglied** der Gruppierung.

- Der dauerhafte Zusammenschluss zweier oder mehreren Gruppierungen und / oder
Fraktionen, sowie Organisationen jeglicher Art ist **strikt** untersagt. Eine Fusion einer Fraktion / Gruppierung / Organisation, welche dauerhaft aufgelöst werden und somit eine
komplett neue Zusammenstellung hervorgeht ist erlaubt.

- Ein nicht anhaltender Zusammenschluss in einem Zeitraum von **einem Tag** , wird nur **alle 3 Tage** gestattet und auch nur dann wenn dies aus einer direkten Reaktion auf eine RP-Aktion geschieht.

- Unangemeldete Gruppierungen (sog. Banden) dürfen **maximal 5 Spieler** umfassen und auch nur mit
diesen **ausgewählten Spielern** ist zu agieren. Diese Regel tritt nur in Kraft sollte dem
Server-Team **kein Konzept** eingereicht worden ist.

- Unangemeldete Gruppierung welche **ein Konzept** eingereicht haben, dürfen mit bis zu **5 Spielern** agieren. Sollte das Konzept **abgelehnt** worden sein, so darf diese Gruppierung nicht mehr als Gruppierung vorgehen und muss sich wieder auf **3 Spieler** begrenzen.

- Angemeldete Gruppierungen müssen von den Administratoren bestätigt werden. Es gibt zum aktuellen Zeitpunkt 5 Plätze für Gruppierungen die angenommen werden. **Unternehmen, Firmen, Staatsfraktionen** oder Ähnliches **fallen nicht** unter diese Regelung.

- Mitglieder angemeldeter Gruppierungen müssen durch Kleidung oder sonstige Merkmale dauerhaft erkennbar sein. Bei einem Gangkrieg **muss** die Kleidung vollständig der Gruppierung angepasst sein. Es muss während eines Krieges dauerhaft erkennbar sein, wer wem zugehörig ist.

- Die Kommunikation innerhalb Fraktionen, Gruppierungen oder Organisationen darf nur per IC-Voice und Funk erfolgen.

- Jede Gruppierung hat für bestimmte Tätigkeiten eine Tagesbegrenzung. So darf jede Organisation **maximal eine** Bank und **maximal drei** Ladenräube pro Tag durchführen.
In Sonderfällen kann dies mit Genehmigung des Server-Teams überschritten werden.