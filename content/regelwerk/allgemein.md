+++
title = "Allgemeine Regeln"
description = ""
+++

- **GOLDENE REGEL:** Das Leben eures RP-Charakters wird so geschützt wie ihr eures im Reallife auch schützen würdet! Behandelt es so, als ob dies euer **einziges** Leben wäre. Jeder Schuss, jede Verletzung, jeder Unfall oder jede übermütige Tat könnte euer Leben beenden.
Somit ist euer Leben euer höchstes &quot;Hab und Gut&quot;. Denkt daran, ihr übernehmt das Leben eures Charakters. (Man sollte also, wenn man von aggressiven / bewaffneten Leuten umzingelt ist, nicht den Halbstarken spielen und anfangen zu schießen)

- Kommt eine Situation zustande, bei der du Gefahr läufst zu sterben, ist es deine Pflicht, dein Leben zu schützen, da dies dein wertvollster Besitz ist. Wenn du diese Regel ignorierst, kann es soweit kommen, dass dein Charakter den **permanenten Tod** erleidet.

- Das Ausnutzen von Bugs, Spielfehlern, unfairen Spielmechaniken (z.B. färbendes Crosshair) oder Lücken im Regelwerk ist verboten und wird mit einem **permanenten** Ausschluss geahndet. Dazu zählt auch etwaiges Duplizieren von Gegenständen etc.

- Das Verwenden von Cheats, Modmenüs oder sonstigen Hacks ist verboten und wird mit einem permanenten Bann bestraft.

- Das Ausloggen oder absichtliche Fliehen aus einer Rollenspielsituation wird als Cheating gewertet und kann in schweren Fällen zu einem permanenten Bann führen.

- Sofern ein Bug gefunden und **einmal** genutzt wurde, muss dieser innerhalb von **6 Stunden** dem Team bekannt gegeben werden. Sofern der Zeitraum nicht eingehalten wird, wird dies als Bug-Abusing verstanden und dementsprechend geahndet. Das Melden eines Bugs **kann** in besonderen Fällen **belohnt** werden.

- Das Mindestalter beträgt **16 Jahre**. Es besteht die Möglichkeit für Ausnahmen, solltest
du die für Roleplay benötigte geistige Reife besitzen.

- Sollte das RP durch einen Server-Neustart oder Verbindungsproblemen unterbrochen werden, hat man sich nach dem Neustart oder Connecten sofort wieder einzufinden und das RP wird fortgesetzt. Ein Neustart oder Reconnecten ist somit als &quot;nicht existent&quot; zu betrachten.

- Nach einer RP-Situation sollten mindestens 5 Minuten vergehen bevor ihr euch ausloggt, dies gilt als Schutz um agierenden Parteien / Personen ihre RP-Situation nicht zu berauben.

- Erstattungsanträge über den Support werden **nur** mit einem Videobeweis oder mit einem Nachweis durch die angezeigte Death-ID im Spiel als gültig betrachtet und auch nur dann
statt gegeben. Dies gilt auch für Gegenstände die durch einen Spielfehler verloren gingen.
Möglichkeiten für Aufnahmen bieten euch NVIDIA ShadowPlay oder Microsoft Game DVR.

- Kommt es im RP zu Fehlverhalten eines Anderen, so führt ihr die Situation bis zu dem Punkt weiter bis diese als abgeschlossen gilt. **Erst dann** kümmert ihr euch um die Erstellung eines Tickets im Support. Somit wird der Spielfluss im Roleplay **nicht** beeinträchtigt oder abrupt, sowie **unnötig** unterbrochen. Außerdem müssen Regelverstöße **binnen 48 Stunden** gemeldet werden. Im Anschluss ist die Bearbeitung dessen leider nicht mehr möglich.

- Die Handlungs- oder Reaktionszeit beträgt **mindestens** 10 Sekunden, sodass es nicht, wie in den meisten Fällen, zu Überreaktionen oder ausartenden Schusswechseln kommt,
sondern die Möglichkeit und Bedenkzeit für vernünftiges und überlegtes RP entsteht.

- Kommt es zu einer Situation in dem ihr euch klar im Nachteil befindet, so seid ihr verpflichtet den Anweisungen der Personen, die in der Überzahl handeln, folge zu leisten.

- Metagaming, also das Nutzen von Informationen, welche euch außerhalb des Roleplays zugetragen wurden, ist untersagt.

- **15 Minuten** vor einem Server-Restart darf keine Geiselnahme oder ein Raub angefangen werden.