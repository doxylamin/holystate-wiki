+++
title = "Charaktere"
description = ""
+++

- Jeder Spieler ist dazu verpflichtet, einen vernünfigten und nachvollziehbaren Roleplay Namen zu nutzen. Bekannte Charaktere dürfen nicht nachgeahmt werden. Wir behalten uns vor, nachträglich Namensänderungen durchzuführen oder den Spieler zu sperren, bis dieser geändert worden ist.

- Es muss sowohl ein Vor- als auch ein Nachname vorhanden sein.

- Doppeldeutige Namen sind nicht gestattet.

- Der gesamte Name des Charakters sollte realistisch gehalten sein und keine Personen des öffentlichen Lebens imitieren.

- Verstümmelungen die einem Charakter zugefügt wurden, verbleiben nur für den aktuellen Restart.

- Sollte dein Charakter sich unrealistisch verhalten und sein Leben dadurch aktiv in Gefahr bringen kann eine Bewusstlosigkeit durch den Support zum Charaktertod erweitert werden.

- Du kannst deinen Charakter durch einen Suizid sterben lassen. Ein Suizid erfordert eine Anmeldung im Support und einen nachvollziehbaren RP Hintergrund.

- Nach einem festgestellten Tod eures Charakters wird dieser vom Support gelöscht. Eigenes Vermögen in Geld- oder Sachwerten darf vor oder nach einem potentiellen Tod oder einer Ausreise nicht ohne die Genehmigung des Supports vererbt werden.

- Solltet Ihr den Tod eures Charakters in Erwägung ziehen, so muss dies zuvor im Support angemeldet werden. Einfache Anträge nach dem Motto "Mir fällt nichts mehr ein und es ist langweilig". Hierbei sind auch Suizide und Hinrichtungen gemeint. Bei einer Hinrichtung **müssen** beide Parteien zustimmen.