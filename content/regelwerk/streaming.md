+++
title = "Streaming / Aufnahmen"
description = ""
+++

- Streaming und Aufnahmen sind auf unserem Server grundsätzlich erlaubt, unterliegt jedoch den nachfolgenden Bedingungen.

- Geheime Orte, also Orte, die erst im Laufe der Spielzeit bekannt werden, dürfen im Stream nicht offensichtlich gezeigt werden.

- Metagaming ist proaktiv mit bestem Wissen und Gewissen zu verhindern.

- Ein Hinweis auf eine laufende GoPro, Dashcam, etc. ist im Roleplay nicht erlaubt.

- In Supportgesprächen ist die Audio des Streams vollständig zu muten.