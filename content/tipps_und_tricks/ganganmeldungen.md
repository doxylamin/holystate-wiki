+++
title = "Fraktionsbewerbungen"
description = ""
weight = 20
+++

Fraktionsbewerbungen
======

Bei uns im Staat ist es normal, dass man sich für die Jobs als Medic, Cop, Mechaniker oder Taxifahrer bewerben muss.

In unserem Staat ist es normal, dass gewisse Jobs einer Bewerbungspflicht unterliegen. Dies ist unter anderem bei der Polizei, dem Rettungsdienst sowie den Mechanikern und Taxifahrern der Fall. Grundsätzlich sollten alle Bewerbungen als Google Docs-Dokument in die jewiligen Channel in unserem [Discord](https://discord.dreamstate.cc) gepostet werden. Andere Firmen und Fraktionen haben die Möglichkeit, Stellenausschreibungen in folgendem Channel zu posten: [#stellenausschreibungen](https://discordapp.com/channels/591070490173374466/619892958413455380)

* [Los Santos Police Dept. - #bewerbung](https://discordapp.com/channels/613092712350220318/624654637760380956)
* [Los Santos Medical Dept. - #lsmc-bewerbung](https://discordapp.com/channels/613092712350220318/624655035468611597)
* **Los Santos Automobile Club** - Kontaktiere den Geschäftsführer des Unternehmens im Spiel
* **Los Santos Automobile Club** - Suche das Jobcenter auf.

Die meisten Berufe haben allerdings eine Altersgrenze, diese reicht zwischen 14 und 18 Jahren. Die jeweiligen Voraussetzungen findest du in den Bewerbungschannels angepinnt. Gangs lassen den Bewerbungsprozess hauptsächlich über das Roleplay laufen. Sofern du also einer Gang wie z.B. Grove, Crips, o.ä. beitreten möchtest, sprecht die jeweiligen OG's im Roleplay an.

Eigene Fraktion anmelden
=========
Um eine eigene Fraktion oder Gruppierung zu gründen, müsst ihr euer Konzept in unserem [Discord Channel](https://discordapp.com/channels/613092712350220318/619987758756397057) einreichen. Hierbei solltet ihr eine gute und ausführliche Bewerbung vorlegen können. Voraussetzung ist **mindestens eine DIN A4 Seite** - vorhandene Bilder etc. zählen nicht zur Seitenlänge hinzu. Weiterhin ist es möglich, eine eigene Firma zu gründen oder Geschäfte wie z.B den Nachtclub zu übernehmen. Auch dazu sollte dann eine Bewerbung im [Discord Channel](https://discordapp.com/channels/613092712350220318/619987758756397057 eingereicht werden.