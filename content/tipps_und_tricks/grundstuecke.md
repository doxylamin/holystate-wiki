+++
title = "Grundstuecke"
description = ""
weight = 20
+++

Häuser & Grundstücke
======
Wenn euch das Interesse für ein eigenes Haus geweckt hat, könnt ihr diese gerne mieten oder käuflich erwerben.

Andere Spieler können diese Häuser dennoch weiterhin erwerben, wundert euch demnach nicht, falls Ihr plötzlich Mitbewohner habt.

Solltet Ihr euch in ein Haus eingemietet oder gekauft haben, könnt ihr euren eigenen Kleiderschrank und ein persönliches Lager nutzen.
In vielen Fällen steht euch auch eine Garage zur Verfügung.

Die Miete für eure Häuser berechnet sich aus einem kleinen Teil des Kaufpreises. Eure Mieten werden (egal, ob Ihr online oder offline seid) täglich um 22:00 Uhr von eurem Bankkonto abgebucht.