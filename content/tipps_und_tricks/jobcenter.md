+++
title = "Jobcenter"
description = ""
weight = 20
+++

Jobs des Jobcenters
======
Alle Jobs des Jobcenters funktionieren auf dem selben Prinzip. Zunächst müsst ihr euren gewünschten Job annehmen.

Auf der Karte ist nun ein Symbol für die Umkleidekabine und gegebenenfalls die Garage des jeweiligen Jobs markiert (SYMBOLE HIER EINFÜGEN). Erster Schritt wäre dort hinzufahren. Bei vielen Jobs wird die Arbeitsuniform verlangt, ohne diese könnt Ihr den Job leider nicht ausführen.

Falls Ihr noch kein eigenes Fahrzeug besitzt oder euer Kofferraum zu klein ist, könnt ich euch für etwas Kleingeld, einen Firmenwagen ausparken. Sobald Ihr die oben aufgeführten Anforderungen erfüllt, kann es auch schon losgehen. Anbei findet ihr noch einmal die Abfolge der einzelnen Berufe.
**INFO:** *Aktuell kann der Kofferraum von Firmenfahrzeugen nicht genutzt werden. Wir arbeiten bereits an einer Problemlösung.*