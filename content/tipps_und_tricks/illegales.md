+++
title = "Illegale Aktivitäten"
description = ""
weight = 20
+++

Illegale Aktivitäten
======
Auf dem Server haben wir auch extra für euch ein paar Geldeinnahmequellen hinzugefügt, die wesentlich mehr als die normalen Jobs einbringen, jedoch nicht ganz ungefährlich und legal sind.

Unser Staat verfügt über ein Cannabis-Feld, sowie über die Möglichkeit LSD zu farmen. Wie auch bei den normalen Jobs, müssen zunächst die jeweiligen Materialien gesammelt werden, um sie später zu verarbeiten und beim Schwarzmarkt gegen Schwarzgeld zu verkaufen.

Das Schwarzgeld muss im Anschluss bei der Geldwäscherei zu sauberem Geld gewaschen werden. Bei Bedarf kann man auch mit dem Schwarzgeld auf dem Waffenschwarzmarkt Waffen erlangen. Zudem gibt es die Möglichkeit, geklaute oder verlorene Kreditkarten zu sammeln. Anders als bisher werden diese nicht verarbeitet sondern müssen gehackt werden.

Alle erhaltenen Gegenstände können im Anschluss beim Schwarzmarkt verkauft werden.