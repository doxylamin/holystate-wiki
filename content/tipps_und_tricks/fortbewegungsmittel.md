+++
title = "Fortbewegungmittel"
description = ""
weight = 20
+++

Führerscheine
======
Um Fahrzeuge oder Luftfahrzeuge zu erwerben, musst du dir zuallererst einen Führerschein und/oder eine Fluglizenz besorgen. Den Führerschein kannst du durch das bestehen eine theoretischen, sowie praktischen Prüfung bei der Fahrschule <img src="/assets/blips/radar_capture_the_flag.png" style="display:inline; height: auto; margin: auto;"> erhalten.

Dort könnt ihr, wenn ihr möchtet, auch einen Motorrad- & LKW-Führerschein machen. Habt ihr die Praktische Prüfung bestanden, könnt ihr euch beim Fahrzeughändler <img src="/assets/blips/radar_getaway_car.png" style="display:inline; height: auto; margin: auto;"> Autos eurer Wahl kaufen.

Ein- und Ausparken könnt ihr eure Fahrzeuge bei jeder auf der Karte markierten Garage.
<!-- Die Fluglizenz bekommt ihr am Flugzeug-Händler für 25.000$ ohne weitere Prüfungen. -->
