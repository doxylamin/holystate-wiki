---
title: Tastenbelegung
weight: 20
---

## Interaktionen
<table>
<thead>
<tr>
<th align="left">Taste</th>
<th align="left">Aktion</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">{{< button href="#" theme="info" >}}F1{{< /button >}}</td>
<td align="left">Telefon</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}F2{{< /button >}}</td>
<td align="left">Inventar</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}F3{{< /button >}}</td>
<td align="left">Interaktionsmenü</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}F5{{< /button >}}</td>
<td align="left">Kofferraum öffnen/schließen</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}F6{{< /button >}}</td>
<td align="left">Jobinteraktionsmenü</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}ENTF{{< /button >}}</td>
<td align="left">Spielerliste</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}U{{< /button >}}</td>
<td align="left">Fahrzeug auf/abschließen</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}G{{< /button >}}</td>
<td align="left">Fahrzeugsteuerung</td>
</tr>
</tbody>
</table>

## Voice Chat
<table>
<thead>
<tr>
<th align="left">Taste</th>
<th align="left">Aktion</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">{{< button href="#" theme="info" >}}ALT{{< /button >}}</td>
<td align="left">Im Funk sprechen</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}H{{< /button >}}</td>
<td align="left">Sprachreichweite einstellen</td>
</tr>
</tbody>
</table>

## Animationen
<table>
<thead>
<tr>
<th align="left">Taste</th>
<th align="left">Aktion</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">{{< button href="#" theme="info" >}}X{{< /button >}}</td>
<td align="left">Hände hoch / Animation abbrechen</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}B{{< /button >}}</td>
<td align="left">Finger zeigen</td>
</tr>
<tr>
<td align="left">{{< button href="#" theme="info" >}}G{{< /button >}}</td>
<td align="left">Animationsrad</td>
</tr>
</tbody>
</table>
